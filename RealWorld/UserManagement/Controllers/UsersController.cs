﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MassTransit;
using UserManagement.Events;
using UserManagement.Providers;

namespace UserManagement.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUserProvider _userProvider;
        private readonly IBus _bus;

        public UsersController(IUserProvider userProvider,IBus bus)
        {
            _userProvider = userProvider;
            _bus = bus;
        }

        [HttpGet]
        [Route("api/users/createuser")]
        public string CreateUser()
        {
            //save user in local db

            _bus.Publish(new UserCreatedEvent() {UserName = "Tom", Email = "tom@google.com"});

            return "create user named Tom";
        }

        [HttpGet]
        [Route("api/users/updateUser")]
        public string UpdateUser()
        {
            _bus.Publish(new UserUpdatedEvent() {UserName = "Jim", Email = "jim@google.com"});

            return "update user named jim";
        }

       
    }
}
