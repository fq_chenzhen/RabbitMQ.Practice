﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace UserManagement.Service.ContainerInstallers
{
    public class GreetingWriterInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<GreetingWriter>().LifestyleTransient());
        }
    }
}