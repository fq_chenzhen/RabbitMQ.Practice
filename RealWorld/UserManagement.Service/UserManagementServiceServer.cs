﻿using Castle.MicroKernel.Registration;
using MassTransit;

namespace UserManagement.Service
{
    public class UserManagementServiceServer
    {
        private readonly IBusControl _bus;

        public UserManagementServiceServer()
        {
            var container = ApplicationBootstrapper.RegisterContainer();
            RegisterBus();
            _bus = container.Resolve<IBusControl>();
        }

        private void RegisterBus()
        {
            ApplicationBootstrapper.Container.Register(
                Component.For<IBus, IBusControl>()
                    .Instance(UserManagementServiceBusConfiguration.BusInstance)
                    .LifestyleSingleton());
        }

        public void Start()
        {
            _bus.Start();
        }

        public void Stop()
        {
            _bus.Stop();
        }
    }
}