﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Pipeline;

namespace UserManagement.Service.Observers
{
    public class ConsumeObserver<T> :
    IConsumeMessageObserver<T>
    where T : class
    {
        Task IConsumeMessageObserver<T>.PreConsume(ConsumeContext<T> context)
        {
            // called before the consumer's Consume method is called
            Console.WriteLine("PreConsume<T>");
            return Task.FromResult(0);
        }

        Task IConsumeMessageObserver<T>.PostConsume(ConsumeContext<T> context)
        {
            // called after the consumer's Consume method was called
            // again, exceptions call the Fault method.
            Console.WriteLine("PostConsume<T>");
            return Task.FromResult(0);
        }

        Task IConsumeMessageObserver<T>.ConsumeFault(ConsumeContext<T> context, Exception exception)
        {
            // called when a consumer throws an exception consuming the message
            Console.WriteLine("ConsumeFault<T>");
            return Task.FromResult(0);
        }
    }
}